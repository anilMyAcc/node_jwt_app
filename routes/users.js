const express = require('express');
const auth = require('../middlewares/auth');
const User = require('../models/user');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');

const router = express.Router();

// User login
router.post('/login', auth.verifyUser, auth.getToken ,(req, res) => {
  // TODO: use mongo db connection to validate user
  console.log("Heeeeeeeeeeeeeeee")
  res.json({
    token: req.token
  })
});

// Check if logged In
router.get('/login', auth.verifyToken, (req, res) => {
  res.json({
    "status": "ok",
    "authData": req.authData
  })
})

// TO add user
router.post('/signup', (req, res) => {
  User.find({ username: req.body.username})
  .exec()
  .then(
    user => {
      if(user.length > 0) {
        return res.status(409).json({
          message: 'user already exits!..'
        });
      }else{
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if(err){
            return res.status(500).json({
              error: err
            })
          }else{
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              username: req.body.username,
              password: hash
            });
            user.save()
            .then(result => {
              console.log(result);
              res.status(201).json({
                message: 'User created'
              })
            })
            .catch(err => {
              console.log(err);
              res.status(500).json({
                error: err
              })
            }) 
          }
        })
      }
    }
  )
})

module.exports = router;
