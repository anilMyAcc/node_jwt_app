const express = require('express');
const Device = require('../models/device');
const Locations = require('../models/location');
const auth = require('../middlewares/auth');
const router = express.Router();

/*
    Returns: list of devices based on count and pagenumber
*/ 
router.get('/', auth.verifyToken, (req, res) => {
    console.log("Request received to list devices .....")
    const resPerPage = req.query.count || 10;
    const page = req.query.page || 1;

    Device.find().skip((resPerPage * page) - resPerPage).limit(resPerPage).exec()
    .then( devices =>{
        console.log(devices)
        res.status(200).json({
            result: devices
        })
    })
    .catch(err => {
        res.sendStatus(500);
    })
})

/*
    Returns: list of device status based on count and pagenumber
*/
router.get('/locations', auth.verifyToken, (req, res) => {
    const resPerPage = req.query.count || 10;
    const page = req.query.page || 1;
    const device_id = req.query.device_id;
    if(!device_id){
        res.status(400).json({
            message: "Device id is field is empty!!"
        })
    } else {
        Locations.find({device: device_id})
        .sort({createdAt: -1})
        .skip((resPerPage * page) - resPerPage)
        .limit(resPerPage)
        .exec()
        .then( locations => {
            res.status(200).json({
                result: locations
            })
        })
        .catch(err => {
            res.sendStatus(500)
        })
    }
})

module.exports = router;