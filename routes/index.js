var express = require('express');
var router = express.Router();

/* TO check if running */
router.get('/', function(req, res, next) {
  res.json({ title: 'Express App Running...' });
});

module.exports = router;
