const mongoose = require('mongoose');

const deviceSchema = mongoose.Schema();

module.exports = mongoose.model('Devices', deviceSchema, 'devices');