const jwt = require('jsonwebtoken');

module.exports = {
    /*
        Checks if user is vaild
    */
    verifyUser: function(req, res, next){
        console.log("Verifying user.....")
        if(req.body.username === process.env.TEST_USER && req.body.password === process.env.TEST_PASSWORD){
            next()
        }else{
            res.status(401).send({
                message: "Invaild credentials !!!"
            })
        }
    },
    /*
        Checks if token is vaild
    */
    verifyToken: function(req, res, next){
        console.log("Verifying token.....")
        const bearerHeader = req.headers['authorization'];
        if(typeof bearerHeader !== 'undefined'){
            const bearer = bearerHeader.split(' ');
            const bearerToken = bearer[1];
            req.token = bearerToken;
            jwt.verify(bearerToken, process.env.SECRET_KEY, (err, authData) => {
                if(err){
                    res.sendStatus(403);
                }else{
                    req.authData = authData;
                    next();
                }
            })
        }else{
            res.sendStatus(403)
        }
    },
    /*
        Returns jwt token
    */
    getToken: function(req, res, next){
        console.log("Creating token.....")
        jwt.sign({user: req.user}, process.env.SECRET_KEY, { expiresIn: "1h" }, (err, token)=>{
            if(err){
                res.status(400).send("Unable to get token..").end()
            }
            req.token = token;
            next()
        })
    }
}
